package main;

import java.awt.EventQueue;

import connection.ConnectionCreator;
import teacherdetail.TeacherDetailController;
import utilities.Constants;
import model.Teacher;
import teacherdetail.TeacherDetailView;
import welcomepage.WelcomePageView;

public class Driver {

	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					// Open the welcome page
//					WelcomePageView frame = new WelcomePageView();
//					frame.setTitle(Constants.TITLE);
//					frame.setVisible(true);
					
					//Open Teacher Detail Window
					new TeacherDetailController(new Teacher(), new TeacherDetailView());
					// Test Connection
					ConnectionCreator.getConnection();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
